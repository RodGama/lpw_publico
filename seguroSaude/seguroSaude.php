<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<title>Rodrigo Carvalho</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<link rel="stylesheet" type="text/css" href="/Css/css.css">
	</head>
	<body>
		<div class="container-fluid">
			<div class="row content">
				<?php include '../shared/sidebar.php';?>
				<div class="col-sm-9" id="section1">
					<h4><small>Rodrigo Carvalho</small></h4>
					<hr>
					 <div class=" col-md-12 col-xs-12 center-block container-fluid">
						<h1 class="center-block">Seguro Sa&uacute;de</h1>
						<form role="form" method="post" action="seguro.php">
							<div class="form-group col-md-8 col-xs-12">
							  <label for="Nome">Nome:</label>
							  <input type="name" class="form-control" required="required" id="name" name="name" placeholder="Digite seu nome">
							</div>
							<div class="form-group col-md-8 col-xs-12">
							  <label for="sel1">Faixa et&aacute;ria:</label>
							  <select class="form-control" name="faixa" id="faixa" required="required">
								<option value="0">20 ou menos</option>
								<option value="1">21~30</option>
								<option value="2">31~40</option>
								<option value="3">41~50</option>
								<option value="4">51~65</option>
								<option value="5">66 ou mais</option>
							  </select>
							</div>
							<div class="form-group col-md-8 col-xs-12">
							  <label class="checkbox-inline"><input type="radio" required name="doenca" value="sim">Portador de doen&ccedil;a pr&eacute;via</label>
							  <label class="checkbox-inline"><input type="radio" required name="doenca" value="nao">N&atilde;o</label>
							</div>
							<div class="form-group col-xs-12 col-md-12 center-block">
							  <button type="submit" class="btn btn-default">Enviar</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>