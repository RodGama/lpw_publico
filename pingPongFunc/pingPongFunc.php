<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<title>Rodrigo Carvalho</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<link rel="stylesheet" type="text/css" href="/Css/css.css">
	</head>
	<body>

		<div class="container-fluid">
			<div class="row content">
				<!--<div class="col-sm-3 sidenav">
					<div class="col-md-12 col-xs-12 center-block">
							<a id="titulo" href="/index.php" type="button">Rodrigo Carvalho</a>
						</div>
						<a id="p1" href="/curriculum/curriculum.php"  type="button" class="btn btn-primary">Curriculum</a>
						<a id="p2" type="button" href="/pingPong/pingPong.php"  class="btn btn-primary">PingPong</a>
						<a id="p3" type="button" href="/pingPongFunc/pingPongFunc.php" class="btn btn-primary">PingPong com Fun&ccedil;&atildeo </a>
						<a id="p4" type="button" href="/pingPlocParam/pingPlocParam.php" class="btn btn-primary">PingPong com Fun&ccedil;&atildeo e parametro</a>
					<br>
				</div>-->
				<?php include '../shared/sidebar.php';?>
				<div class="col-sm-9" id="section1">
					<h4><small>Rodrigo Carvalho</small></h4>
					<hr>
					<h2>Ping Pong com Fun&ccedil;&atildeo</h2>
					<?php 
					
						function numeroParaTexto( $a ){
							if (($a % 3 == 0) && ($a % 5 == 0)) {
							echo "$a.<span class='ploc'>ploc</span><br>";
							}
							elseif ($a % 3 == 0){
								echo "$a.<span class='ping'>ping</span><br>";
							}
							elseif ($a % 5 == 0) {
								echo "$a.<span class='pong'>pong</span><br>";
							}
							else {
								echo "$a.ok<br>";
								}
						}
							$i = 1;
							for($i=1;$i<100;$i++){
								numeroParaTexto($i);
							}
						?>
					<br><br>
				</div>
			</div>
		</div>

	</body>
</html>